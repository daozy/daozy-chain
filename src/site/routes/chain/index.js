const fs = require("fs")
const url = require('url');
const router = require('express').Router();
const log = require("../log").logger();
const DaozyUtils = require("../daozy_utils");
const EOSApi = require("./eos_api");

const daozy_utils = new DaozyUtils();
const eos_api = new EOSApi();

const module_name = 'chain';
const module_ch_name = 'Daozy Chain';
const page_prefix = module_name + '/';

router.get('/', function(req, res, next) {
    log.info(req.originalUrl);

    var parameters = {
        title: module_ch_name,
        username: 'test',
        alias: 'test',
        role: 'user'
    };

    res.render(page_prefix + "index", parameters);
});

router.get('/:page', function(req, res, next) {
    log.info(req.originalUrl);

    var parameters = {
        title: module_ch_name,
        username: 'test',
        alias: 'test',
        role: 'user'
    };

    var page = req.params['page'];
    res.render(page_prefix + page, parameters);
});

router.post('/', function(req, res, next) {
    try {
        var action = req.body.action;
    } catch (err) {
        log.error(err);
        daozy_utils.send_warn_resp(res, 'parameter action not exist');
        return;
    }

    log.info("==============req.url = %s, action = %s===============", req.originalUrl, action);
    
    // 构造数据进行回复
    if (action == 'get_action_list') {
        daozy_utils.send_data_resp(res, { 
            name_list: ['listproducers', 'listproducers_summary', 'listproducers_ex', 'createkey', 'createwallet', 'deletewallet', 'openwallet', 'lockwallet', 'unlockwallet', 'importkey', 'createaccount'], 
            examples: {
                listproducers: '{ "action": "listproducers", "parameters": { "max": 10, "json": true, "lower": 1 }}',
                listproducers_summary: '{ "action": "listproducers_summary", "parameters": { "max": 10, "lower": 1 }}',
                listproducers_ex: '{ "action": "listproducers_ex", "parameters": { "max": 10, "lower": 1 }}',
            }
        });
    } else if (action == 'listproducers') {
        var parameters = daozy_utils.parser_para(res, req.body.parameters);
        if (!parameters) {
            return;
        }       
         
        eos_api.listproducers(parameters.max, parameters.json, parameters.lower, (err, ret) => {
            if (err) {
                log.error(err);
                daozy_utils.send_warn_resp(res, "查询失败，请联系管理员");
            } else {
                daozy_utils.send_data_resp(res, ret);
            }
        });
    } else if (action == 'listproducers_summary') {
        var parameters = daozy_utils.parser_para(res, req.body.parameters);
        if (!parameters) {
            return;
        }         
         
        eos_api.listproducers_summary(parameters.max, parameters.lower, (err, ret) => {
            if (err) {
                log.error(err);
                daozy_utils.send_warn_resp(res, "查询失败，请联系管理员");
            } else {
                daozy_utils.send_data_resp(res, ret);
            }
        });
    } else if (action == 'listproducers_ex') {
        var parameters = daozy_utils.parser_para(res, req.body.parameters);
        if (!parameters) {
            return;
        }         
         
        eos_api.listproducers_ex(parameters.max, parameters.lower, (err, ret) => {
            if (err) {
                log.error(err);
                daozy_utils.send_warn_resp(res, "查询失败，请联系管理员");
            } else {
                daozy_utils.send_data_resp(res, ret);
            }
        });
    } else {
        daozy_utils.send_warn_resp(res, "action不可识别");
    } 
});

module.exports = router;
