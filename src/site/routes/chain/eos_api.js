var fs = require("fs");
var url = require('url');
var router = require('express').Router();
const log = require("../log").logger();

const { spawn } = require('child_process');

const node_http_addr = 'http://node2.liquideos.com:8888';
const node_https_addr = 'https://node2.liquideos.com';

function EOSApi() {
    this.listproducers = function(max, json, lower, callback) {
        var params = ['-u', node_https_addr, 'system', 'listproducers', '-l', max];
        if (json) {
            params.push("-j");
        }
        
        if (lower) {
            params.push("-L", lower);
        }
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
		  //log.info(`stdout: ${data}`);
		  callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
		  //log.error(`stderr: ${data}`);
		  callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
		  //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
     * @brief: list all producers(summary)
	 * @param [in] max: the max number of producers that will be returned
     * @param [in] lower: lower bound value of key
	 * @param [in] callback: cb(err, data)
	 * @return
        example:
        Producer      Producer key                                           Url                                                         Scaled votes
eoshuobipool  EOS5XKswW26cR5VQeDGwgNb5aixv1AMcKkdDNrC59KzNSBfnH6TR7  http://eoshuobipool.com                                     0.0271 // 投票占比（百分比）
eoscannonchn  EOS73cTi9V7PNg4ujW5QzoTfRSdhH44MPiUJkUV6m3oGwj7RX7kML  https://eoscannon.io/                                       0.0267
zbeosbp11111  EOS7rhgVPWWyfMqjSbNdndtCK8Gkza3xnDbUupsPLMZ6gjfQ4nX81  http://www.zbeos.com                                        0.0260
eosnewyorkio  EOS6GVX8eUqC1gN1293B3ivCNbifbr1BT6gzTFaQBXzWH9QNKVM4X  https://bp.eosnewyork.io                                    0.0253
eosisgravity  EOS55HTTjoxVX1zVpW8pabxygBb1J3SEnG5D8D3y3KgrnSbLpELfE  http://eosgravity.com                                       0.0248
eosauthority  EOS4va3CTmAcAAXsT26T3EBWqYHgQLshyxsozYRgxWm9tjmy17pVV  https://eosauthority.com                                    0.0241
cypherglasss  EOS5rTrUiqvgu7YCVyKCeQ1QXA7Uo94FZhq7zKcNPqbrCP5u5fQXo  http://cypherglass.com                                      0.0234
eosiomeetone  EOS5gS4ZtanRS2Jx4vpjAQaVNci3v65iZiGCgMr9DNwu67x2pt8Qd  https://meet.one                                            0.0227
cryptolions1  EOS5yHaUBwhpwFftNjADdVwBpCVybLPg5P3z7HpTbDGjUfpqwWSSf  http://CryptoLions.io                                       0.0226
eosbeijingbp  EOS5dGpcEhwB4VEhhXEcqtZs9EQj5HeetuXDnsAGVHMXHAFdMjbdj  https://www.eosbeijing.one                                  0.0224
-L eosstorebest for more // 未在该次返回中列出的下一个生产者（降序排列），想要接着查询后面的生产者，则将该名字通过"lower"参数传入
     */
    this.listproducers_summary = function(max, lower, callback) {
		this.listproducers(max, lower, null, callback)
	}
    
    /*
	 * @brief: list all producers(extend)
	 * @param [in] max: the max number of producers that will be returned
     * @param [in] lower: lower bound value of key
	 * @param [in] callback: cb(err, data)
	 * @return
        example:
        {
        "rows": [{
        "owner": "zbeosbp11111",        // 其中一个生产者
        "total_votes": "224678796171560928.00000000000000000", // 票数(投票权重)
        "producer_key": "EOS7rhgVPWWyfMqjSbNdndtCK8Gkza3xnDbUupsPLMZ6gjfQ4nX81", // 生产者公钥
        "is_active": 1,                 
        "url": "http://www.zbeos.com",  // url
        "unpaid_blocks": 11748,
        "last_claim_time": "1530201589500000",
        "location": 0
        },{
        "owner": "eosnewyorkio",        // 另一个生产者
        "total_votes": "220056814792401536.00000000000000000",
        "producer_key": "EOS6GVX8eUqC1gN1293B3ivCNbifbr1BT6gzTFaQBXzWH9QNKVM4X",
        "is_active": 1,
        "url": "https://bp.eosnewyork.io",
        "unpaid_blocks": 4410,
        "last_claim_time": "1530278617500000",
        "location": 184
        },{
        "owner": "eoshuobipool",
        "total_votes": "218894186583713760.00000000000000000",
        "producer_key": "EOS5XKswW26cR5VQeDGwgNb5aixv1AMcKkdDNrC59KzNSBfnH6TR7",
        "is_active": 1,
        "url": "http://eoshuobipool.com",
        "unpaid_blocks": 6588,
        "last_claim_time": "1530255842000000",
        "location": 0
        },{
        "owner": "eoscannonchn",
        "total_votes": "217558857692342016.00000000000000000",
        "producer_key": "EOS73cTi9V7PNg4ujW5QzoTfRSdhH44MPiUJkUV6m3oGwj7RX7kML",
        "is_active": 1,
        "url": "https://eoscannon.io/",
        "unpaid_blocks": 6900,
        "last_claim_time": "1530252554000000",
        "location": 0
        },{
        "owner": "eosauthority",
        "total_votes": "208368013980252128.00000000000000000",
        "producer_key": "EOS4va3CTmAcAAXsT26T3EBWqYHgQLshyxsozYRgxWm9tjmy17pVV",
        "is_active": 1,
        "url": "https://eosauthority.com",
        "unpaid_blocks": 6612,
        "last_claim_time": "1530255598500000",
        "location": 826
        },{
        "owner": "cypherglasss",
        "total_votes": "203138764883974464.00000000000000000",
        "producer_key": "EOS5rTrUiqvgu7YCVyKCeQ1QXA7Uo94FZhq7zKcNPqbrCP5u5fQXo",
        "is_active": 1,
        "url": "http://cypherglass.com",
        "unpaid_blocks": 7980,
        "last_claim_time": "1530241202000000",
        "location": 0
        },{
        "owner": "eosisgravity",
        "total_votes": "199120495904047904.00000000000000000",
        "producer_key": "EOS55HTTjoxVX1zVpW8pabxygBb1J3SEnG5D8D3y3KgrnSbLpELfE",
        "is_active": 1,
        "url": "http://eosgravity.com",
        "unpaid_blocks": 3072,
        "last_claim_time": "1530292738000000",
        "location": 0
        },{
        "owner": "eosliquideos",
        "total_votes": "191378834318021760.00000000000000000",
        "producer_key": "EOS4v1n2j5kXbCum8LLEc8zQLpeLK9rKVFmsUgLCWgMDN38P6PcrW",
        "is_active": 1,
        "url": "http://vote.liquideos.com/",
        "unpaid_blocks": 6276,
        "last_claim_time": "1530259128500000",
        "location": 0
        },{
        "owner": "eosiomeetone",
        "total_votes": "190620557538584800.00000000000000000",
        "producer_key": "EOS5gS4ZtanRS2Jx4vpjAQaVNci3v65iZiGCgMr9DNwu67x2pt8Qd",
        "is_active": 1,
        "url": "https://meet.one",
        "unpaid_blocks": 528,
        "last_claim_time": "1530319497500000",
        "location": 0
        },{
        "owner": "eoscanadacom",
        "total_votes": "190511739598035424.00000000000000000",
        "producer_key": "EOS5HYV7rWeRxpZMCooe8YHRFQHKK7ncdmmUMTe3wCMaY2EvyVzUx",
        "is_active": 1,
        "url": "https://www.eoscanada.com",
        "unpaid_blocks": 3060,
        "last_claim_time": "1530292894000000",
        "location": 0
        }
        ],
        "total_producer_vote_weight": "8248775948627019776.00000000000000000",    // 总票数（投票权重）
        "more": "cryptolions1"  // 未在该次返回中列出的下一个生产者（降序排列），想要接着查询后面的生产者，则将该名字通过"lower"参数传入
        }

	 */
	this.listproducers_ex = function(max, lower, callback) {
		this.listproducers(max, lower, "true", callback)
	}
    
    // ======================== wallet ============================ 
    /*
	 * @brief: create key
	 * @param [in] callback: cb(err, data)
	 * @return
        example:
        Private key: 5KGT2frWccfFw883HMXV1tHmEcVwbLDw577VntahAz2LxA1Zw8m
        Public key: EOS6YLNyGy8Cr8pRXZ4YtnJyr644yVuyVqmRyWTD6gDmhCC8rEGvC
     */ 
    this.createkey = function(callback) {
        var params = ['create', 'key'];
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
		  //log.info(`stdout: ${data}`);
		  callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
		  //log.error(`stderr: ${data}`);
		  callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
		  //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
	 * @brief: create wallet
	 * @param [in] name: the name of wallet
     * @param [in] callback: cb(err, data)
	 * @return
        example:
        Creating wallet: test_wallet_1
        Save password to use in the future to unlock this wallet.
        Without password imported keys will not be retrievable.
        "PW5J3DovFuc4JsJUvJvQaaKGBo4y97WZQYKLU3DamqvoqLJCEdCQv" // the password of wallet
     */ 
    this.createwallet = function(name, callback) {
        var params = ['wallet', 'create', '-n', name];
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
		  //log.info(`stdout: ${data}`);
		  callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
		  //log.error(`stderr: ${data}`);
		  callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
		  //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
	 * @brief: delete wallet
	 * @param [in] name: the name of wallet
     * @param [in] callback: cb(err, data)
	 * @return
        example:
        
     */ 
    this.deletewallet = function(name, callback) {
        var params = ['/root/eosio-wallet/' + name + '.wallet', '-rf'];
        
		const cmd = spawn('rm', params);
		cmd.stdout.on('data', (data) => {
		  //log.info(`stdout: ${data}`);
		  callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
		  //log.error(`stderr: ${data}`);
		  callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
		  //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
	 * @brief: open wallet
	 * @param [in] name: the name of wallet
     * @param [in] callback: cb(err, data)
	 * @return
        example:
        Opened: test_wallet_1   // test_wallet_1 is opened.
     */ 
    this.openwallet = function(name, callback) {
        var params = ['wallet', 'open', '-n', name];
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
		  //log.info(`stdout: ${data}`);
		  callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
		  //log.error(`stderr: ${data}`);
		  callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
		  //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
	 * @brief: lock wallet
	 * @param [in] name: the name of wallet
     * @param [in] callback: cb(err, data)
	 * @return
        example:
        
     */ 
    this.lockwallet = function(name, callback) {
        var params = ['wallet', 'lock', '-n', name];
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
		  //log.info(`stdout: ${data}`);
		  callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
		  //log.error(`stderr: ${data}`);
		  callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
		  //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
	 * @brief: unlock wallet
	 * @param [in] name: the name of wallet
     * @param [in] pwd: the password of wallet
     * @param [in] callback: cb(err, data)
	 * @return
        example:
        
     */ 
    this.unlockwallet = function(name, pwd, callback) {
        var params = ['wallet', 'unlock', '-n', name, '--password', pwd];
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
            //log.info(`stdout: ${data}`);
            callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
            //log.error(`stderr: ${data}`);
            callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
            //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
	 * @brief: import key
	 * @param [in] name: the name of wallet
     * @param [in] name: private key
     * @param [in] callback: cb(err, data)
	 * @return
        example:
        imported private key for: EOS7dLgCB8T3Gc9EDV44RApW5j3EYqUfiWSmSqq4DS7k6HjuifLKt  // display the public key corresponding to the private key
     */ 
    this.importkey = function(name, private_key, callback) {
        var params = ['wallet', 'import', '-n', name, private_key];
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
            //log.info(`stdout: ${data}`);
            callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
            //log.error(`stderr: ${data}`);
            callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
            //log.info(`child process exited with code ${code}`);
		});
	}
    
    /*
	 * @brief: create account
	 * @param [in] name: the name of wallet
     * @param [in] name: private key
     * @param [in] callback: cb(err, data)
	 * @return
        example:
        
     */ 
    this.createaccount = function(creator, name, owner_key, active_key, stake_net, stake_cpu, buy_ram, callback) {
        var params = ['system', 'newaccount', '-u', node_https_addr, creator, name, owner_key, active_key,
                     '--stake-net', stake_net, '--stake-cpu', stake_cpu, '--buy-ram', buy_ram];
        
		const cmd = spawn('cleos', params);
		cmd.stdout.on('data', (data) => {
            //log.info(`stdout: ${data}`);
            callback(null, `${data}`);
		});

		cmd.stderr.on('data', (data) => {
            //log.error(`stderr: ${data}`);
            callback(`${data}`, null);
		});

		cmd.on('close', (code) => {
            //log.info(`child process exited with code ${code}`);
		});
	}
}

module.exports = EOSApi;
