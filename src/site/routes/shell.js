const fs = require("fs");
const log = require("./log").logger();

function Shell() {}

module.exports = Shell;

Shell.rm = function rm(path) {
	if (fs.existsSync(path)) {
		remove(path);
	} else {
		logger.error('path: %s not exist', path)
	}

	function remove(path) {
		if (fs.statSync(path).isDirectory()) {
		    var files = fs.readdirSync(path);
		    files.forEach(function(file, index) {
		        remove(path + "/" + file);
		    });
		    fs.rmdirSync(path);    		
		} else {
			fs.unlinkSync(path);
		}
	}
}

Shell.copy = function copy(src_path, dst_path) {
	if (fs.existSync()) {
		shell.rm(dst_path)
		copy_file(src_path, dst_path);
	} else {
		logger.error('src path: %s not exist');
	}

	function copy_file(src_path, dst_path) {
		if (fs.statSync(src_path).isDirectory()) {
			create_dir(dst_path)
			var files = fs.readdirSync(src_path);
			files.forEach(function(file, index) {
				var cur_src_path = src_path + '/' + file;
				var cur_dst_path = dst_path + '/' + file;
				copy_file(cur_src_path, cur_dst_path);
			});
		} else {
			var readable = fs.createReadSteam(src_path);
			var writeable = fs.createWriteStream(dst_path);
			readable.pipe(writeable);
		}
	}

	function create_dir(path) {
		if (fs.existSync(path)) {
			return;
		}

		fs.mkdirSync(path);
	}
}