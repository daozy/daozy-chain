var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var log = require("./log").logger();
var error_map = require("./error");

function ImServer() {}

module.exports = ImServer;

var onlineUsers = {};

ImServer.response = function response(username, data) {
	if (onlineUsers.hasOwnProperty(username)) {
		onlineUsers[username]["socket"].emit("response", data);
	}
}

ImServer.error = function error(err) {
	var err_desc = "";

	if (typeof err != 'string') {
		err_desc = error_map["001"];
	} else {
		err_desc = err;
	}

	for (var key in onlineUsers) {
		var res_error = {
			action: "sys_error",
			user_name: key,
			error: err_desc
		};

		// log.info("user = %s", key);

		onlineUsers[key]["socket"].emit("response", res_error);
	}
}

http.listen(8890, function() {
	log.info('IM server listening on *:8890');
});

io.sockets.on('connection', function(socket) {

	log.info('user connected im server');

	socket.on('request', function(data) {
		log.info('im action = %s', data.action);
		if (data.action == "login") {
			socket.name = data.username;
			if(!onlineUsers.hasOwnProperty(data.username)) {
				onlineUsers[data.username] = { "username": data.username, "socket": socket };
			}
			
			log.info("user:%s login im server", data.username);

		} else {
			if(!onlineUsers.hasOwnProperty(data.username)) {
				log.error("user:%s don't login im server", data.username);
				return;
			}

			if (data.actname == "test") {
				log.info("received user:%s action:%s", data.username, data.actname);	
			} else {
				log.info("action = %s don't exist", data.actname);
			}
		}
	});
	
	socket.on('disconnect', function() {
		if (onlineUsers.hasOwnProperty(socket.name)) {			
			delete onlineUsers[socket.name];
			log.info("user:%s logout", socket.name);
		}
	});
});