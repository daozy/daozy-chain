var fs = require("fs")
var url = require('url');
var router = require('express').Router();
const log = require("./log").logger();

const DEBUG = true;

function DaozyUtils() {
	/*
	 * brief: 解析POST参数到JSON格式
	 * @param [in] res: 请求
	 * @param [in] data: post请求
	 * return JSON格式参数
	 */	
	this.parser_para = function(res, data) {
		try {
			var ret = JSON.parse(data);
		} catch (err) {
			log.error(err);
			try {
				this.send_warn_resp(res, "请求的参数格式错误");
			    return null;
			} catch (err) {
				log.error(err);
				return null;
			}
		} 

		if (DEBUG) {
	  		log.info(ret);
	  	}

	  	return ret;
	}

	/*
	 * brief: 回复客户端不带参数，或者回复错误消息
	 * @param [in] res: reponse
	 * @param [in] err: 错误消息，如果为空表示没有错误
	 */	
	this.send_resp = function(res, err) {
		try {
			if (err) {
				res.status(404).json({error: err});
			} else {
				res.status(200).json({result:true});
			}
		} catch (err) {
			log.error(err);
		}
	}

	/*
	 * brief: 回复客户端提示信息
	 * @param [in] res: reponse
	 * @param [in] err: 提示消息
	 */	
	this.send_warn_resp = function(res, err) {
		try {
		    res.status(200).json({
		      	result: false,
		      	error: err
		    });
		} catch (err) {
			log.error(err);
		}
	}	

	/*
	 * brief: 带参数回复客户端
	 * @param [in] res: reponse
	 * @param [in] err: 错误，如果为空，表示没有出错
	 * @param [in] res_data: 回复数据
	 */	
	this.send_data_resp = function(res, res_data, err) {
		try {
			if (err) {
				res.status(404).json({
				  	error: err
				});
			} else {
				if (res_data == null) {
					res_data = [];
				}
				
				if (DEBUG) {
					log.info(res_data);
				}

				res.status(200).json({
				  	result: true,
				  	data: res_data
				});
			}
		} catch (err) {
			log.error(err);
		}
	}

	/*
	 * brief: 判断对象是否为空
	 * @param [in] obj: 对象
	 * return 
	 */		
	this.obj_is_empty = function(obj) {
		return obj.id;
	}
}

module.exports = DaozyUtils;